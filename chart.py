# -*- coding: utf-8 -*-
"""
Created on Sun Jun  6 17:52:36 2021

@author: caleb
"""


import csv

def insert_table(csvfile):
    with open(csvfile,newline='',encoding = 'utf-8-sig') as file:
        rows = csv.DictReader(file)
        final_list = []
        
        for row in rows:
            new_dict = {}
            for key in rows.fieldnames:
                new_dict[key] = row[key]
            final_list.append(new_dict)
            #print(row)
            #print(row['姓名'],row['SID'],row['性別'],row['活力排'])
        
        return final_list
    
#if __name__ == '__main__':
#    insert_table("shepherd.csv")