# -*- coding: utf-8 -*-
"""
Created on Sun Jun  6 21:18:53 2021

@author: caleb
"""

import pymysql
import chart
# 資料庫參數設定

class DB():
    def __init__(self):
        db_settings = {
            "host": "127.0.0.1",
            "port": 3306,
            "user": "root",
            "password": "S!1verLug!@",
            "db": "newman",
            "charset": "utf8"
        }
        self.conn = pymysql.connect(**db_settings)

    def insert_init_data(self):
        # Insert data
        conn = self.conn
        with conn.cursor() as cursor:
            ## Insert data of 牧人
            '''
            command = "INSERT INTO 牧人(姓名,SID,性別,活力排) values (%s,%s,%s,%s)"
            rows = chart.insert_table("shepherd.csv")
            for row in rows:
                cursor.execute(command,(row['姓名'],row['SID'],row['性別'],row['活力排']))
        conn.commit()
        '''
        
            ## Insert data of sheep
            '''
            command = "INSERT INTO sheep(姓名,SID,性別,牧人SID) values (%s,%s,%s,%s)"
            rows = chart.insert_table("sheep.csv")
            for row in rows:
                cursor.execute(command,(row['姓名'],row['SID'],row['性別'],row['牧人SID']))
        conn.commit()
        '''
            ## Insert data of gathering
            '''
            command = "INSERT INTO gathering(名稱,天,時間,負責弟兄SID) values (%s,%s,%s,%s)"
            rows = chart.insert_table("gathering.csv")
            for row in rows:
                cursor.execute(command,(row['名稱'],row['天'],row['時間'],row['負責弟兄SID']))
        conn.commit()
        '''
            ## Insert data of vital group
            '''
            command = "INSERT INTO vital_group(名稱,書報,晨興時間) values (%s,%s,%s)"
            rows = chart.insert_table("vital_group.csv")
            for row in rows:
                cursor.execute(command,(row['名稱'],row['書報'],row['晨興時間']))
        conn.commit()
        '''
            ## Insert data of function group
            '''
            command = "INSERT INTO func_group(名稱,負責弟兄1_SID,負責弟兄2_SID,交通時間,天) values (%s,%s,%s,%s,%s)"
            rows = chart.insert_table("func_group.csv")
            for row in rows:
                cursor.execute(command,(row['名稱'],row['負責弟兄1_SID'],row['負責弟兄2_SID'],row['交通時間'],row['天']))
        conn.commit()
            '''
            
            ## Insert data of function group
            '''
            command = "INSERT INTO service(SID,功能組,身份) values (%s,%s,%s)"
            rows = chart.insert_table("service.csv")
            for row in rows:
                cursor.execute(command,(row['SID'],row['功能組'],row['身份']))
        conn.commit()
        '''
             ## Insert data of function group
            
            command = "INSERT INTO attend(SID,聚會,邀請人) values (%s,%s,%s)"
            rows = chart.insert_table("attend.csv")
            for row in rows:
                cursor.execute(command,(row['SID'],row['聚會'],row['邀請人']))
        conn.commit()
        

    def cmd_exec(self,command):
        try:
            with self.conn.cursor() as cursor:
                cursor.execute(command)
                result = cursor.fetchall()
                col = cursor.description
                self.conn.commit()
                return result2Dict(col,result)
                
        except Exception as ex:
            print(ex)
            return str(ex)
            
    def retrieveKeyByCommand(self,command):
        try:
            with self.conn.cursor() as cursor:
                cursor.execute(command)
                col = cursor.description
                self.conn.commit()
                return col
        except Exception as ex:
            print(ex)
            return str(ex)
    
    # def select_from_where(conn,target,table,cond):
    #     with conn.cursor() as cursor:
    #         command = "SELECT 
    
def result2Dict(col,result):
    if result:
        d = {}
        key = []
        
        for i in range(len(col)):
            key.append(col[i][0])
            
        for i in range(len(result[0])):
            value = []
            for j in range(len(result)):
                value.append(result[j][i])
            d[key[i]] = value
        
        return d
    else:
        print("Nothing")
        return None

def tempForDebug():
    db = DB()
    command = "SELECT 活力排, COUNT(*) FROM shepherd GROUP BY 活力排;"
    d = db.cmd_exec(command)
    return d

if __name__ == "__main__":
    db = DB()
    #db.insert_init_data()
    command = "SELECT 活力排, COUNT(*) FROM shepherd GROUP BY 活力排;"
    d = db.cmd_exec(command)
    #k = db.retrieveKeyByCommand(command)
    