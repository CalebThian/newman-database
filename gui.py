# -*- coding: utf-8 -*-
"""
Created on Mon Jun  7 08:51:19 2021

@author: caleb
"""

import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QFrame, QComboBox, QSplitter, QPushButton, QTextEdit, QWidget, QLineEdit, QLabel,QGridLayout, QHBoxLayout,QVBoxLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
import db
import table

class gui(QWidget):
    def __init__(self):
        super().__init__()
        self.none_text = "------------------"
        self.cal_able_type = [0,1,2,3,4,5,7,8,9,10,11,12,13,14,246,247,249,250,251,252]
        self.database = db.DB()
        self.data = {}
        self.window()
        
    def window(self):
        #3 sublayout
        ## a. choose query or button
        func = ["MySQL","SELECT-FROM-WHERE","DELETE","INSERT","UPDATE"]
        n_queries = ["IN","NOT IN","EXISTS","NOT EXISTS"]
        agg_func = ["COUNT","SUM","MAX","MIN","AVG","HAVING"]
        self.table_list = ["shepherd","sheep","gathering","vital_group","service","func_group","attend"]
        
        
        # Set the layout as horizontal
        layout = QVBoxLayout()
        
        ## Create button area
        btn_grid = QGridLayout()
        
        ### Create Frame for each basic function
        topleft = QFrame()
        topright = QFrame()
        btmleft = QFrame()
        btmright = QFrame()
        
        topleft.setFrameShape(QFrame.StyledPanel)
        topright.setFrameShape(QFrame.StyledPanel)
        btmleft.setFrameShape(QFrame.StyledPanel)
        btmright.setFrameShape(QFrame.StyledPanel)
        
        
        btn_grid.addWidget(topleft,0,0,3,1)
        btn_grid.addWidget(topright,0,1,2,1)
        btn_grid.addWidget(btmleft,2,1)
        btn_grid.addWidget(btmright,0,2,3,1)
        
        ### Set corresponding frame layout
        topleft.setLayout(self.SELECT_FROM_WHERE_layout())
        topright.setLayout(self.DELETE_layout())
        btmleft.setLayout(self.INSERT_layout())
        btmright.setLayout(self.UPDATE_layout())
        
        ## Set button size
        #sfw_btn.resize(sfw_btn.sizeHint())
        #sfw_btn.clicked.connect(self.sfw_btn_clicked)
        
        # delete_btn.resize(delete_btn.sizeHint())
        # delete_btn.clicked.connect(self.delete_btn_clicked)
        
        # insert_btn.resize(insert_btn.sizeHint())
        # insert_btn.clicked.connect(self.insert_btn_clicked)
        
        # insert_btn.resize(insert_btn.sizeHint())
        # insert_btn.clicked.connect(self.insert_btn_clicked)
        
        
        # Create command area
        mysql_lbl = QLabel("MySQL: ")
        self.cmd = QTextEdit()
        exe = QPushButton("Execute ")
        exe.resize(exe.sizeHint())
        exe.clicked.connect(self.exe_clicked)
        
        mysql_lbl_layout = QVBoxLayout()
        mysql_lbl_layout.addWidget(mysql_lbl)
        mysql_lbl_layout.addStretch(1)
    
        pb_layout = QVBoxLayout()
        pb_layout.addStretch(1)
        #pb_layout.addWidget(exe)
        
        cmd_layout = QHBoxLayout()
        cmd_layout.addLayout(mysql_lbl_layout)
        cmd_layout.addWidget(self.cmd)
        cmd_layout.addLayout(pb_layout)
        
        # SQL Command 
        self.cmd_run = QLabel("Command: ")
        self.error = QLabel("Error:")
        
        # Result table
        self.tb = table.TableView(self.data,4,3)
        
        # Arrange all widget and layout together
        layout.addLayout(btn_grid)
        layout.addLayout(cmd_layout)
        layout.addWidget(exe)
        layout.addWidget(self.cmd_run)
        layout.addWidget(self.error)
        layout.addWidget(self.tb)
        
        # Show Widget
        self.setLayout(layout)
        self.setGeometry(250,30,1500,1000)
        self.setWindowTitle("光鹽唱詩社社員管理系統")
        self.show()
              
    def exe_clicked(self):
        command = str(self.cmd.toPlainText())
        print(command)
        self.cmd_run.setText("Command: "+command)
        self.execute(command)

    def execute(self,command):
        ans = self.database.cmd_exec(command)
        if isinstance(ans,str):
            self.error.setText("Error: "+ ans)
        else:
            self.error.setText("Error: ")
        if "SELECT" in command:
            self.tb.resetData(ans)
            
    def execute_s(self,command):
        ans = self.database.cmd_exec(command)
        if "SELECT" in command:
            self.tb.resetData(ans)
        
    def retrieve_cursor_description(self,table_name):
        command = "SELECT * FROM "+table_name
        keys = self.database.retrieveKeyByCommand(command)
        return keys
    
    def SELECT_FROM_WHERE_layout(self):
        ## SELECT-FROM-WHERE area
        sfw_layout = QGridLayout()
        
        ### Create widget for SFW area
        sfw_lbl = QLabel("1. SELECT-FROM-WHERE")
        sfw_btn = QPushButton("QUERY")
        
        ### Grid area
        sfw_enter = QGridLayout()
        
        #### Create widget for grid area
        s_lbl = QLabel("SELECT")
        self.s_cmb = QComboBox()
        self.s_cmb.addItem(self.none_text)
        self.s_led = QLineEdit()
        
        f_lbl = QLabel("FROM")
        self.f_cmb = QComboBox()
        for t in self.table_list: 
            self.f_cmb.addItem(t)
        self.f_cmb.addItem("Custom")
        self.f_led = QLineEdit()
        
        w_lbl = QLabel("WHERE")
        self.w_led = QLineEdit()
        
        gb_lbl = QLabel("GROUP BY:")
        self.gb_led = QLineEdit()
        
        #### Nested Queries
        self.in_cmb = QComboBox()
        self.in_cmb.addItem(self.none_text)
        
        self.nq_cmb = QComboBox()
        self.nq_func = ["NONE","IN","NOT IN","EXISTS","NOT EXISTS"]
        for func in self.nq_func:
            self.nq_cmb.addItem(func)
        
        sq_lbl = QLabel("SELECT")
        self.sq_cmb = QComboBox()
        self.sq_cmb.addItem(self.none_text)
        self.sq_led = QLineEdit()
        
        fq_lbl = QLabel("FROM")
        self.fq_cmb = QComboBox()
        for t in self.table_list: 
            self.fq_cmb.addItem(t)
        self.fq_cmb.addItem("Custom")
        self.fq_led = QLineEdit()
        
        wq_lbl = QLabel("WHERE")
        self.wq_led = QLineEdit()
        
        #### Insert into grid
        sfw_enter.addWidget(s_lbl,0,0)
        sfw_enter.addWidget(self.s_cmb,0,1,1,2)
        sfw_enter.addWidget(self.s_led,0,3,1,2)
        sfw_enter.addWidget(f_lbl,1,0)
        sfw_enter.addWidget(self.f_cmb,1,1,1,2)
        sfw_enter.addWidget(self.f_led,1,3,1,2)
        sfw_enter.addWidget(w_lbl,2,0)
        sfw_enter.addWidget(self.w_led,2,1,1,4)
        sfw_enter.addWidget(gb_lbl,3,0)
        sfw_enter.addWidget(self.gb_led,3,1,1,4)
        sfw_enter.addWidget(self.in_cmb,4,0)
        sfw_enter.addWidget(self.nq_cmb,4,1)
        sfw_enter.addWidget(sq_lbl,4,2)
        sfw_enter.addWidget(self.sq_cmb,4,3)
        sfw_enter.addWidget(self.sq_led,4,4)
        sfw_enter.addWidget(fq_lbl,5,0)
        sfw_enter.addWidget(self.fq_cmb,5,1)
        sfw_enter.addWidget(self.fq_led,5,2,1,3)
        sfw_enter.addWidget(wq_lbl,6,0)
        sfw_enter.addWidget(self.wq_led,6,1,1,4)
        
        ### Insert into SFW area
        sfw_layout.addWidget(sfw_lbl,0,0)
        sfw_layout.addLayout(sfw_enter,1,0,6,1)
        sfw_layout.addWidget(sfw_btn,8,0)
        
        ### Connect
        self.f_cmb.currentTextChanged.connect(self.on_f_cmb_changed)
        self.nq_cmb.currentTextChanged.connect(self.on_nq_cmb_changed)
        self.fq_cmb.currentTextChanged.connect(self.on_fq_cmb_changed)
        sfw_btn.clicked.connect(self.on_sfw_btn_clicked)
        
        ### Initialize
        self.on_f_cmb_changed(str(self.f_cmb.currentText()))
        self.on_nq_cmb_changed(str(self.nq_cmb.currentText()))
        self.on_fq_cmb_changed(str(self.fq_cmb.currentText()))
        
        return sfw_layout
    
    def on_f_cmb_changed(self,value):
        self.s_cmb.clear()
        if value != "Custom":
            keys = self.retrieve_cursor_description(value)
            
            self.s_cmb.addItem("*")
            self.s_cmb.addItem("COUNT(*)")
            for i in range(len(keys)):
                self.s_cmb.addItem(keys[i][0])
                if keys[i][1] in self.cal_able_type:
                    self.s_cmb.addItem("SUM("+keys[i][0]+")")
                    self.s_cmb.addItem("MAX("+keys[i][0]+")")
                    self.s_cmb.addItem("MIN("+keys[i][0]+")")
                    self.s_cmb.addItem("AVG("+keys[i][0]+")")
        
        self.s_cmb.addItem("Custom")
                    
    def on_nq_cmb_changed(self,value):
        self.in_cmb.clear()
        if self.f_cmb.currentText() != "Custom":
            cur_table = str(self.f_cmb.currentText())
        else:
            cur_table = self.f_led.text()
            
        if "IN" in value:
            keys = self.retrieve_cursor_description(cur_table)
            
            for i in range(len(keys)):
                self.in_cmb.addItem(keys[i][0])
        else:
            self.in_cmb.addItem(self.none_text)
                
    def on_fq_cmb_changed(self,value):
        self.sq_cmb.clear()
        if value != "Custom":
            keys = self.retrieve_cursor_description(value)
            
            for i in range(len(keys)):
                self.sq_cmb.addItem(keys[i][0])
        else:
            self.sq_cmb.addItem("Custom")
        
    def on_sfw_btn_clicked(self):
        command = "SELECT "
        if self.s_cmb.currentText() != "Custom":
            command = command + self.s_cmb.currentText()
        else:
            command = command + self.s_led.text()

        command = command + " FROM "
        if self.f_cmb.currentText() != "Custom":
            command = command + self.f_cmb.currentText()
        else:
            command = command + self.f_led.text()
        
        
        if self.w_led.text() != "" or self.nq_cmb.currentText() != "NONE":
            command = command + " WHERE "
            if self.w_led.text() != "":
                command = command + self.w_led.text()
            if self.nq_cmb.currentText() != "NONE":
                if self.in_cmb.currentText() != self.none_text:
                    command = command + " " + self.in_cmb.currentText()
                command = command + " " + self.nq_cmb.currentText() + " (SELECT "
                
                if self.sq_cmb.currentText() != "Custom":
                    command = command + self.sq_cmb.currentText()
                else:
                    command = command + self.sq_led.text()
            
                command = command + " FROM "
                if self.fq_cmb.currentText() != "Custom":
                    command = command + self.fq_cmb.currentText()
                else:
                    command = command + self.fq_led.text()
                
                if self.wq_led.text() != "" :
                    command = command + " WHERE " + self.wq_led.text()
                command = command + ")"
                
        if self.gb_led.text() != "":
                command = command + " GROUP BY " + self.gb_led.text()     
        
        command = command + ";"
        print(command)
        self.cmd_run.setText(command)
        self.execute(command)
            
        
        
    def DELETE_layout(self):
        ## DELETE area
        del_layout = QVBoxLayout()
        
        ### Create widget for SFW area
        del_lbl = QLabel("2. DELETE")
        del_btn = QPushButton("DELETE")
        
        ### Grid area
        del_enter = QGridLayout()
        
        #### Create widget for grid area
        df_lbl = QLabel("DELETE FROM")
        self.df_cmb = QComboBox()
        for t in self.table_list: 
            self.df_cmb.addItem(t)
        self.df_cmb.addItem("Custom")
        self.df_led = QLineEdit()
        
        dw_lbl = QLabel("WHERE")
        self.dw_led = QLineEdit()
        
        #### Nested Queries
        self.din_cmb = QComboBox()
        self.din_cmb.addItem(self.none_text)
        
        self.dnq_cmb = QComboBox()
        for func in self.nq_func:
            self.dnq_cmb.addItem(func)
        
        dsq_lbl = QLabel("SELECT")
        self.dsq_cmb = QComboBox()
        self.dsq_cmb.addItem(self.none_text)
        self.dsq_led = QLineEdit()
        
        dfq_lbl = QLabel("FROM")
        self.dfq_cmb = QComboBox()
        for t in self.table_list: 
            self.dfq_cmb.addItem(t)
        self.dfq_cmb.addItem("Custom")
        self.dfq_led = QLineEdit()
        
        dwq_lbl = QLabel("WHERE")
        self.dwq_led = QLineEdit()
        
        #### Insert into grid
        del_enter.addWidget(df_lbl,0,0)
        del_enter.addWidget(self.df_cmb,0,1,1,2)
        del_enter.addWidget(self.df_led,0,3,1,2)
        del_enter.addWidget(dw_lbl,1,0)
        del_enter.addWidget(self.dw_led,1,1,1,4)
        del_enter.addWidget(self.din_cmb,2,0)
        del_enter.addWidget(self.dnq_cmb,2,1)
        del_enter.addWidget(dsq_lbl,2,2)
        del_enter.addWidget(self.dsq_cmb,2,3)
        del_enter.addWidget(self.dsq_led,2,4)
        del_enter.addWidget(dfq_lbl,3,0)
        del_enter.addWidget(self.dfq_cmb,3,1)
        del_enter.addWidget(self.dfq_led,3,2,1,3)
        del_enter.addWidget(dwq_lbl,4,0)
        del_enter.addWidget(self.dwq_led,4,1,1,4)
        ### Insert into SFW area
        del_layout.addWidget(del_lbl)
        del_layout.addLayout(del_enter)
        del_layout.addWidget(del_btn)
        
        ### Connect
        self.dnq_cmb.currentTextChanged.connect(self.on_dnq_cmb_changed)
        self.dfq_cmb.currentTextChanged.connect(self.on_dfq_cmb_changed)
        del_btn.clicked.connect(self.on_df_btn_clicked)
        
        
        return del_layout

    def on_dnq_cmb_changed(self,value):
        self.din_cmb.clear()
        if self.df_cmb.currentText() != "Custom":
            cur_table = str(self.df_cmb.currentText())
        else:
            cur_table = self.df_led.text()
            
        if "IN" in value:
            keys = self.retrieve_cursor_description(cur_table)
            
            for i in range(len(keys)):
                self.din_cmb.addItem(keys[i][0])
        else:
            self.din_cmb.addItem(self.none_text)
                
    def on_dfq_cmb_changed(self,value):
        self.dsq_cmb.clear()
        if value != "Custom":
            keys = self.retrieve_cursor_description(value)
            
            for i in range(len(keys)):
                self.dsq_cmb.addItem(keys[i][0])
        else:
            self.dsq_cmb.addItem("Custom")
        
    def on_df_btn_clicked(self):
        command = "DELETE FROM "
        if self.df_cmb.currentText() != "Custom":
            command = command + self.df_cmb.currentText()
        else:
            command = command + self.df_led.text()
        
        if self.dw_led.text() != "" or self.dnq_cmb.currentText() != "NONE":
            command = command + " WHERE "
            if self.dw_led.text() != "":
                command = command + self.dw_led.text()
            if self.dnq_cmb.currentText() != "NONE":
                if self.din_cmb.currentText() != self.none_text:
                    command = command + " " + self.din_cmb.currentText()
                command = command + " " + self.dnq_cmb.currentText() + " (SELECT "
                
                if self.dsq_cmb.currentText() != "Custom":
                    command = command + self.dsq_cmb.currentText()
                else:
                    command = command + self.dsq_led.text()
            
                command = command + " FROM "
                if self.dfq_cmb.currentText() != "Custom":
                    command = command + self.dfq_cmb.currentText()
                else:
                    command = command + self.dfq_led.text()
                
                if self.dwq_led.text() != "" :
                    command = command + " WHERE " + self.dwq_led.text()
                command = command + ")"
        command = command + ";"
        print(command)
        self.cmd_run.setText(command)
        self.execute(command)
        
        ### Run SELECT to see result
        keyword1 = "FROM"
        keyword2 = "WHERE"
        cor_table = command[command.find(keyword1)+len(keyword1)+1:command.find(keyword2)-1]
        cond = command[command.find(keyword2)+len(keyword2)+1:]
        command2 = "SELECT * FROM "+cor_table+";"
        print(command2)
        self.execute_s(command2)
        
    
    def INSERT_layout(self):
        ## INSERT area
        ins_layout = QVBoxLayout()
        
        ### Create widget for SFW area
        ins_lbl = QLabel("3. INSERT")
        ins_btn = QPushButton("INSERT")
        
        ### Grid area
        ins_enter = QGridLayout()
        
        #### Create widget for grid area
        ii_lbl = QLabel("INSERT INTO")
        self.ii_cmb = QComboBox()
        for t in self.table_list: 
            self.ii_cmb.addItem(t)
        
        val_lbl = QLabel("values")
        self.val_led = QLineEdit()
        
        #### Insert into grid
        ins_enter.addWidget(ii_lbl,0,0)
        ins_enter.addWidget(self.ii_cmb,0,1,1,2)
        ins_enter.addWidget(val_lbl,1,0)
        ins_enter.addWidget(self.val_led,1,1,1,2)
        
        ### Insert into SFW area
        ins_layout.addWidget(ins_lbl)
        ins_layout.addLayout(ins_enter)
        ins_layout.addWidget(ins_btn)
        
        ### Connect
        ins_btn.clicked.connect(self.on_ins_btn_clicked)
        
        return ins_layout
    
    def on_ins_btn_clicked(self):
        command = "INSERT INTO " + self.ii_cmb.currentText()
    
        command = command + " values(" + self.val_led.text() + ");"
        print(command)
        self.cmd_run.setText(command)
        self.execute(command)
        
        ### Run SELECT to see result
        keyword1 = "INTO"
        keyword2 = "values("
        cor_table = command[command.find(keyword1)+len(keyword1)+1:command.find(keyword2)-1]
        command2 = "SELECT * FROM "+cor_table+";"
        print(command2)
        self.execute_s(command2)
    
    def UPDATE_layout(self):
        ## UPDATE area
        upd_layout = QGridLayout()
        
        ### Create widget for SFW area
        upd_lbl = QLabel("4. UPDATE")
        upd_btn = QPushButton("UPDATE")
        
        ### Grid area
        upd_enter = QGridLayout()
        
        #### Create widget for grid area
        u_lbl = QLabel("UPDATE")
        self.u_cmb = QComboBox()
        for t in self.table_list: 
            self.u_cmb.addItem(t)
        
        set_lbl = QLabel("SET")
        self.set_led = QLineEdit()
        
        uw_lbl = QLabel("WHERE")
        self.uw_led = QLineEdit()
        
        #### Nested Queries
        self.uin_cmb = QComboBox()
        self.uin_cmb.addItem(self.none_text)
        
        self.unq_cmb = QComboBox()
        for func in self.nq_func:
            self.unq_cmb.addItem(func)
        
        usq_lbl = QLabel("SELECT")
        self.usq_cmb = QComboBox()
        self.usq_cmb.addItem(self.none_text)
        self.usq_led = QLineEdit()
        
        ufq_lbl = QLabel("FROM")
        self.ufq_cmb = QComboBox()
        for t in self.table_list: 
            self.ufq_cmb.addItem(t)
        self.ufq_cmb.addItem("Custom")
        self.ufq_led = QLineEdit()
        
        uwq_lbl = QLabel("WHERE")
        self.uwq_led = QLineEdit()
        
        #### Insert into grid
        upd_enter.addWidget(u_lbl,0,0)
        upd_enter.addWidget(self.u_cmb,0,1,1,4)
        upd_enter.addWidget(set_lbl,1,0)
        upd_enter.addWidget(self.set_led,1,1,1,4)
        upd_enter.addWidget(uw_lbl,2,0)
        upd_enter.addWidget(self.uw_led,2,1,1,4)
        
        upd_enter.addWidget(self.uin_cmb,3,0)
        upd_enter.addWidget(self.unq_cmb,3,1)
        upd_enter.addWidget(usq_lbl,3,2)
        upd_enter.addWidget(self.usq_cmb,3,3)
        upd_enter.addWidget(self.usq_led,3,4)
        upd_enter.addWidget(ufq_lbl,4,0)
        upd_enter.addWidget(self.ufq_cmb,4,1)
        upd_enter.addWidget(self.ufq_led,4,2,1,3)
        upd_enter.addWidget(uwq_lbl,5,0)
        upd_enter.addWidget(self.uwq_led,5,1,1,4)
        
        ### Insert into SFW area
        upd_layout.addWidget(upd_lbl,0,0)
        upd_layout.addLayout(upd_enter,1,0,6,1)
        upd_layout.addWidget(upd_btn,8,0)
        
        ### Connect
        self.unq_cmb.currentTextChanged.connect(self.on_unq_cmb_changed)
        self.ufq_cmb.currentTextChanged.connect(self.on_ufq_cmb_changed)
        upd_btn.clicked.connect(self.on_upd_btn_clicked)
        
        return upd_layout
    
    def on_unq_cmb_changed(self,value):
        self.uin_cmb.clear()
        cur_table = str(self.u_cmb.currentText())
            
        if "IN" in value:
            keys = self.retrieve_cursor_description(cur_table)
            
            for i in range(len(keys)):
                self.uin_cmb.addItem(keys[i][0])
        else:
            self.uin_cmb.addItem(self.none_text)
                
    def on_ufq_cmb_changed(self,value):
        self.usq_cmb.clear()
        if value != "Custom":
            keys = self.retrieve_cursor_description(value)
            
            for i in range(len(keys)):
                self.usq_cmb.addItem(keys[i][0])
        else:
            self.usq_cmb.addItem("Custom")
    
    def on_upd_btn_clicked(self):
        command = "UPDATE " + self.u_cmb.currentText()
        command = command + " SET " + self.set_led.text()
        if self.uw_led.text() != "" or self.unq_cmb.currentText() != "NONE":
            command = command + " WHERE "
            if self.uw_led.text() != "":
                command = command + self.uw_led.text()
            if self.unq_cmb.currentText() != "NONE":
                if self.uin_cmb.currentText() != self.none_text:
                    command = command + " " + self.uin_cmb.currentText()
                command = command + " " + self.unq_cmb.currentText() + " (SELECT "
                
                if self.usq_cmb.currentText() != "Custom":
                    command = command + self.usq_cmb.currentText()
                else:
                    command = command + self.usq_led.text()
            
                command = command + " FROM "
                if self.ufq_cmb.currentText() != "Custom":
                    command = command + self.ufq_cmb.currentText()
                else:
                    command = command + self.ufq_led.text()
                
                if self.uwq_led.text() != "" :
                    command = command + " WHERE " + self.uwq_led.text()
                command = command + ")"
        command = command + ";"
        print(command)
        self.cmd_run.setText(command)
        self.execute(command)
        
        ### Run SELECT to see result
        keyword1 = "UPDATE"
        keyword2 = "SET"
        keyword3 = "WHERE"
        cor_table = command[command.find(keyword1)+len(keyword1)+1:command.find(keyword2)-1]
        cond = command[command.find(keyword3)+len(keyword3)+1:]
        command2 = "SELECT * FROM "+cor_table+" "+cond
        print(command2)
        self.execute_s(command2)
        
    
def main():
    app = QApplication(sys.argv)
    g = gui()
    sys.exit(app.exec_())
    
if __name__ == '__main__':
    main()    