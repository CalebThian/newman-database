# -*- coding: utf-8 -*-
"""
Created on Thu Jun 10 10:17:04 2021

@author: caleb
"""

from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QAction, QTableWidget,QTableWidgetItem,QVBoxLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
import sys
import db

data = {'col1':['1','2','3','4'],
        'col2':['1','2','1','3'],
        'col3':['1','1','2','1']}

data2 = {'col1':['1','2','3','4'],
        'col2':['1','2','1','3']}
 
class TableView(QTableWidget):
    def __init__(self, data, *args):
        QTableWidget.__init__(self, *args)
        self.data = data
        self.setData()
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
 
    def setData(self): 
        horHeaders = []
        for n, key in enumerate(sorted(self.data.keys())):
            horHeaders.append(key)
            for m, item in enumerate(self.data[key]):
                item = str(item)
                newitem = QTableWidgetItem(item)
                self.setItem(m, n, newitem)
                print(m,n,newitem)
        self.setHorizontalHeaderLabels(horHeaders)
        
    def resetData(self,data):
        self.clear()
        keys = list(data.keys())
        self.setColumnCount(len(keys))
        self.setRowCount(len(data[keys[0]]))
        self.data = data
        self.setData()
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
    
def main(args):
    app = QApplication(sys.argv)
    table = TableView(data,16,16)
    table.resetData(data2)
    table.show()
    sys.exit(app.exec_())
    
if __name__=="__main__":
    main(sys.argv)

 